# Script verifica se o dia atual é útil com base no arquivo "feriados.csv" . 
# Se sim, fará o download via wget e disponibilizará em um determinado diretório para 
# o consumo da ferramenta da ???? para inserção das informações contidas no
# arquivo no banco de dados.
# Caso não, o script é finalizado automaticamente.
#
# O arquivo "feriados.csv" foi disponibilizado no site oficial da Anbima contendo informações sobre
# todos os feriados nacionais até o ano 2078.
#

import datetime
import csv 
import wget
import os
import sys
import shutil
import time
import logging
import calendar
import smtplib
from email.mime.text import MIMEText

# Função responsável por criar diretórios se não existirem
def createDir(diretorio):
	if(os.path.exists(diretorio)):
		logger.info('Diretório "{}" existe' .format(diretorio))
	else:
		logger.info('Criando diretório "{}"' .format(diretorio))
		os.mkdir(diretorio)


# Função responsável pelo envio de e-mail
def sendMail(text: str):

	# Assunto do e-mail
	SUBJECT = ''

	SMTP_HOST = ''
	SMTP_PORT = 

	USERNAME = ''
	PASSWORD = ''

	FROM 	 = ''
	TO 		 = ['']

	# Mensagem do e-mail
	MESSAGE 			= MIMEText(text)
	MESSAGE['subject'] 	= SUBJECT
	MESSAGE['from'] 	= FROM
	MESSAGE['to'] 		= ', '.join(TO)
	
	# Tentativa de envio de e-mail
	try:
		
		# Conexão com o servidor
		server = smtplib.SMTP_SSL(SMTP_HOST, SMTP_PORT)

		# Login no servidor
		# server.login(USERNAME, PASSWORD)

		# Enviando e-mail
		server.sendmail(FROM, TO, MESSAGE.as_string())
	
		# Saindo do servidor
		server.quit()

		logger.info("E-mail enviado com sucesso para " + MESSAGE['to'])

	except smtplib.SMTPAuthenticationError as e:
		print("Problemas na autententicacao do usuario {}" .format(USERNAME))
		logger.error("E-MAIL: Problemas na autententicacao do usuario {}" .format(USERNAME))
	except smtplib.SMTPNotSupportedError as e:
		print("Autententicacao nao suportada no servidor SMTP")
		logger.error("E-MAIL: Autententicacao nao suportada no servidor SMTP")
	except smtplib.SMTPException as e:
		print("Problemas ao enviar e-mail para {}" .format(MESSAGE['to']))
		logger.error("E-MAIL: Falha ao enviar e-mail para {}" .format(MESSAGE['to']))
	except Exception as e:
		# print(e)
		erro(e)


def moveFile(filename, diretorio):
	msg = ''

	try:
		shutil.move(filename, diretorio+filename)
		logger.info('MOVE: Arquivo "{}" foi movido para o diretório "{}"' .format(filename, diretorio))
	except FileNotFoundError as e:
		msg = 'Arquivo "{}" ou diretorio "{}" nao existe' .format(arquivo, diretorio)
		print(msg)
		logger.error(msg)
		sendMail(msg)
		return 0
	except PermissionError as e:
		msg = 'MOVE: Permissao negada ao mover arquivo "{}" para o diretorio "{}"' .format(arquivo, diretorio)
		print(msg)
		logger.error(msg)
		sendMail(msg)
	except Exception as e:
		print(e)
		erro(e)


# Função responsável por exibir o erro no log
def erro(e):
	e = str(e)
	e = e.split(']')
	e = e[1].strip()
	print(e)
	logger.error(e)


# Diretório de arquivos temporários
dirTmp = 'temp/' 

# Diretório onde o ??? irá consumir o arquivo
dirProcess = 'process/'

# Diretório do arquivo csv
dirCsv = ''

# Diretório do log
dirLog = 'log/'

# Diretório da flag que o Mapping gera
dirFlag = 'flag/'

# Criação do log
DATA_EXEC = time.strftime("%Y")
os.makedirs(os.path.dirname(dirLog), exist_ok=True)
logging.basicConfig(level=logging.INFO,
	format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
	datefmt='%d/%m/%Y - %H:%M:%S',
	filename=dirLog+'anbima_'+ DATA_EXEC +'.log', filemode='a')
logger = logging.getLogger(sys.argv[0])

logger.info('Script acionado')

# createDir(dirLog)
# createDir(dirTmp)
# createDir(dirProcess)

# Informações do dia atual
atual 	= datetime.datetime.now()
numWeek = int(atual.strftime('%w'))
dia 	= int(atual.strftime('%d'))
mes 	= int(atual.strftime('%m'))
ano 	= int(atual.strftime('%Y'))

hoje = atual.strftime('%y%m%d')

# URL na qual o script fará o download do arquivo
url = ''

with open(dirCsv+'feriados.csv', 'r', newline = '') as arq:
	arquivo = csv.reader(arq, delimiter = ',')

	for linha in reversed(list(arquivo)):
		diaArq = int(linha[0].split('/')[0])
		mesArq = int(linha[0].split('/')[1])
		anoStrArq = str(linha[0].split('/')[2])

		anoArq = int(anoStrArq)+2000

		feriado = datetime.datetime(anoArq, mesArq, diaArq).strftime('%y%m%d')

		# print(str(hoje)+' => '+str(feriado))

		if((numWeek == 0) or (numWeek == 6) or (hoje == feriado)): # Verifica se dia atual é sabado, domingo ou feriado
			print('feriado')
			logger.info('O dia atual não é útil')
			logger.info('Script finalizado')
			logger.info('------------------------------------------------------------------------------------')
			exit(0)
		else:
			continue

hoje = atual.strftime('%Y%m%d')

filename = hoje+"teste"

link = url+filename

arquivoRecebido = hoje+'_recebido.txt'

if ((dia == 1) and (mes == 1)):
	mes = 12
	ano = ano-1
	ultimoDiaMes = calendar.monthrange(ano, mes)
	dia = ultimoDiaMes[1]
elif (dia == 1):
	ultimoDiaMes = calendar.monthrange(ano, mes-1)
	dia = ultimoDiaMes[1]
	mes = mes-1
else: 
	dia = dia-1

anterior = datetime.datetime(ano, mes, dia)
numWeek = int(anterior.strftime('%w'))
dia = int(anterior.strftime('%d'))
mes = int(anterior.strftime('%m'))

anterior = int(anterior.strftime('%y%m%d'))

# Consulta arquivo "feriados.csv"
with open(dirCsv+'feriados.csv', 'r', newline = '') as arq:
	arquivo = csv.reader(arq, delimiter = ',')

	for linha in reversed(list(arquivo)):
		diaArq 	= int(linha[0].split('/')[0])
		mesArq 	= int(linha[0].split('/')[1])
		anoArq 	= int(linha[0].split('/')[2])+2000

		feriado = datetime.datetime(anoArq, mesArq, diaArq).strftime('%y%m%d')

		# Percorre os dias anteriores verificando se é útil
		while((numWeek == 0) or (numWeek == 6) or (str(anterior) == feriado)):
			if ((dia == 1) and (mes == 1)):
				mes = 12
				ano = ano-1
				ultimoDiaMes = calendar.monthrange(ano, mes)
				dia = ultimoDiaMes[1]
			elif (dia == 1):
				ultimoDiaMes = calendar.monthrange(ano, mes-1)
				dia = ultimoDiaMes[1]
				mes = mes-1
			else: 
				dia = dia-1

			anterior = datetime.datetime(ano, mes, dia)
			numWeek = int(anterior.strftime('%w'))
			dia     = int(anterior.strftime('%d'))
			anterior = int(anterior.strftime('%y%m%d'))

anterior = datetime.datetime(ano, mes, dia)
anterior = int(anterior.strftime('%Y%m%d'))

print(anterior)

arquivoAux = str(anterior)+"teste"

print(arquivoAux)

count = 0

for _, _, arquivos in os.walk(dirFlag):  
	for arquivo in arquivos:
		count = count + 1

print(count)

if count != 0:
	for _, _, arquivos in os.walk(dirFlag):
		for arquivo in arquivos:
			print(arquivoAux)
			if (arquivo == str(arquivoAux)+"_OK" or arquivo == str(arquivoAux)+"_ERRO"):
				print(arquivo)
				try:
					os.remove(dirFlag+arquivo)
					logger.info('DELETE: Flag "{}" foi excluída com sucesso' .format(arquivo))
				except Exception as e:
					erro(e)
			# else:
			# 	logger.error('Flag "{}" possui nome inválido no diretório "{}"' .format(arquivo, dirFlag))
			# 	sendMail('Flag "{}" possui nome inválido no diretório "{}"' .format(arquivo, dirFlag))
else:
	logger.warning('Nenhuma flag no diretório')

# Exclui flag de sucesso das datas anteriores
for _, _, arquivos in os.walk(dirTmp):
	
	for arquivo in arquivos:
		data = arquivo.split('_')[0]
		print(data)

		if(data != hoje):
			try:
				os.remove(dirTmp+arquivo)
				logger.info('DELETE: A flag "{}" foi excluída com sucesso' .format(arquivo))
			except FileNotFoundError as e:
				logger.warning('DELETE: Flag "{}" não foi encontrada para ser excluída' .format(arquivo))

# Verifica de arquivo já foi baixado, se sim finaliza o programa
try:
	if os.path.isfile(dirTmp+arquivoRecebido):
		logger.info('Flag "{}" já existe' .format(arquivoRecebido))
		logger.info('Script finalizado')
		logger.info('------------------------------------------------------------------------------------')
		exit(0)
	else:
		download = wget.download(link) # Faz download caso arquivo não exista
		logger.info('WGET: Acessando URL {}' .format(link))
		logger.info('WGET: Realizando download do arquivo {}' .format(filename))
		# shutil.move(filename, dirProcess+filename)
		moveFile(filename, dirProcess)
		# logger.info('MOVE: Arquivo "{}" foi movido para o diretório "{}"' .format(filename, dirProcess))
		flag = open(dirTmp+arquivoRecebido, 'w', newline = '')
		flag.close()
		
	if os.path.isfile(dirTmp+'erro.txt'): # Verifica se flag de erro existe, se sim a exclui
		try:
			os.remove(dirTmp+'erro.txt')
			logger.info('DELETE: A flag "erro.txt" foi excluída com sucesso')
		except Exception as e:
			erro(e)

except Exception as e:
	
	print("\nERRO")

	text = 'O arquivo {} ainda não está disponível para download.' .format(filename)

	# print(text)

	sendMail(text)

	logger.info('WGET: Acessando URL {}' .format(link))
	flag = open(dirTmp+'erro.txt', 'w', newline = '') # Caso ocorra algum problema na tentativa de download, uma flag de erro é criada
	flag.write(text)
	flag.close()
	logger.error(text)
	logger.info('Criando flag de erro no diretorio "{}"' .format(dirTmp))

	
logger.info('Script finalizado')
logger.info('------------------------------------------------------------------------------------')

